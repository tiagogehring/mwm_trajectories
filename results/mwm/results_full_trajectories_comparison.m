function results_full_trajectories_comparison
%RESULTS_FULL_TRAJECTORIES_CLASSIFICATION Compare classification of
%trajectories with a manual classification

    % global data initialized elsewhere
    global g_config;
    global g_trajectories; 
    global g_segments_classification;
    
    cache_trajectories;     
    cache_trajectories_classification;
    
          
    % load trajectory tags -> these are the tags assigned to the full
    % trajectories
    
    param = g_config.TAGS_CONFIG{ g_config.TAGS_FULL };    

    [full_labels_data, full_tags] = g_trajectories.read_tags(param{1}, g_config.TAG_TYPE_BEHAVIOUR_CLASS);
    full_map = g_trajectories.match_tags(full_labels_data, full_tags);        
    
    % select only tagged trajectories
    tagged = sum(full_map, 2) > 0;
           
    res = g_segments_classification;
    
    segments = res.segments;
    partitions = segments.partitions;

    % map segment to full trajectory tags
    tag_map = repmat({}, 1, length(full_tags));
    for k = 1:length(full_tags)
        % look for tags matching the current one                
        tag_map{k} = tag.tag_position(res.classes, full_tags(k).abbreviation);
        if tag_map{k} == 0
            mess = sprintf('\nclass not present in classification: %s', full_tags(k).description);
            disp(mess);
        end
    end

    % select only trajectories with at least 2 segments
    part_idx = find(partitions > 0);

    % convert from individual segment classes to full trajectories
    % distribution of classes
    seg_map = res.mapping_ordered();

    err2 = zeros(1, length(full_tags));
    tot = zeros(1, length(full_tags));
    distr = zeros(1, length(full_tags));

    for k = 1:length(part_idx)
        if ~tagged(part_idx(k))
            continue;
        end
        % ntags = 0;
        % count # of tags not present in the full trajectories                
        for l = 1:length(full_tags)
            t = tag_map{l};                      
            if t(1) ~= 0
                % ntags = ntags + results.class_map(k, l);
                distr(l) = sum(seg_map(k, :) == t);
                
                % add also 
                if l == 5
                    distr(l) = distr(l) + sum(seg_map(k, :) == 5);
                    distr(l) = distr(l) + sum(seg_map(k, :) == 6);
                end
                
                if l == 7
                    distr(l) = distr(l) + sum(seg_map(k, :) == 7);
                end                    
            end
        end

     %   [~, sorting] = sort(distr, 'descend');
        for l = 1:length(full_tags)
            t = tag_map{l};                      
            if t(1) ~= 0
                if full_map(part_idx(k), l)
                    % test if tag present
                    tot(l) = tot(l) + 1;

                    if distr(l) == 0
                        mess = sprintf('trajectory (set %d, day %d, trk %d): tag %s not detected', ...
                            g_trajectories.items(part_idx(k)).set, g_trajectories.items(part_idx(k)).session, ...
                            g_trajectories.items(part_idx(k)).track, full_tags(l).description);                                                    
                        disp(mess);
                        err2(l) = err2(l) + 1;                                
                    else
                        % test if the class is one of the main ones in the
                        % distribution
%                                 if sorting(l) > ntags
%                                     fprintf('trajectory (set %d, day %d, trk %d): tag %s not a major one\n', ...
%                                         sel_traj.items(traj_idx(k)).set, sel_traj.items(traj_idx(k)).session, sel_traj.items(traj_idx(k)).track, full_tags(l).description);                        
%                                     err2 = err2 + 1;
%                                 end
                    end
                end
            end
        end
    end

    for k = 1:length(full_tags)
        % look for tags matching the current one                            
        if tag_map{k} ~= 0
            mess = sprintf('\nTag %s: tot: %d, err: %d (%f)', full_tags(k).description, tot(k), err2(k), 100.*err2(k)/tot(k));
            disp(mess);
        end
    end                  
end