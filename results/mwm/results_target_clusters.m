function retults_target_clusters
% CACHE_TRAJECTORIES_CLASSIFICATION
%   Loads trajectories, parititions and then classify them using the
%   default parameters

    % load trajectories and segments
    global g_segments;        
    global g_config;
    global g_partitions;    
    global g_trajectories;
    cache_trajectory_segments;
    
    % parameters of the most detailed set
    param = g_config.TAGS_CONFIG{2};    
        
    coverage = [];
    for nc = param{2} - 20:param{2} + 15    
        % get classifier object
        classif = g_segments.classifier(param{1}, g_config.DEFAULT_FEATURE_SET, g_config.TAG_TYPE_BEHAVIOUR_CLASS);
        
        % classify segments
        res = classif.cluster(nc, 0);    
        
        coverage = [coverage, res.coverage];        
    end   
    
    
end
