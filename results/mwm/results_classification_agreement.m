function results_classification_agreement
    global g_config;
    global g_trajectories_classification_results;
    
    cache_trajectories_classification;
    
    % the time bins (in seconds) for which we will do the comparision
    binsz = .2;
    bins = repmat(binsz, 1, 90/binsz);
    
    for i = 2:length(g_trajectories_classification_results)
        res1 = g_trajectories_classification_results(i);
        distr1 = res1.mapping_time(bins, 'DiscardUnknown', 0);
        
        for j = 1:i - 1         
            res2 = g_trajectories_classification_results(j);
            distr2 = res2.mapping_time(bins, 'DiscardUnknown', 0);
            
            n = 0;
            match = 0;
            diff_bins = [];          
            diff_idx = 0;
            
            for k = 1:size(distr1, 1)                
                for l = 1:size(distr1, 2)                
                    if distr1(k, l) == 0 && distr2(k, l) == 0
                        if diff_idx ~= 0
                            diff_bins = [diff_bins, l - diff_idx];
                            diff_idx = 0;
                        end
                    else
                        n = n + 1;
                        if distr1(k, l) == distr2(k, l) || distr1(k, l) == 0 || distr2(k, l) == 0
                            match = match + 1;
                            if diff_idx ~= 0
                                diff_bins = [diff_bins, l - diff_idx];
                                diff_idx = 0;
                            end
                        else
                            if diff_idx == 0
                                diff_idx = l;
                            end
                        end                                                
                    end
                end                
                if diff_idx ~= 0
                    diff_bins = [diff_bins, l - diff_idx];
                    diff_idx = 0;
                end
            end
            
            %[diff_set, diff_len] = res2.difference(res1, 'SegmentTolerance', 51);
            aggr = 100.*match/n;                        
            diff_bins = diff_bins*binsz;
            
            fprintf('\nAGREEMENT OF CLASSIFICATIONS %d and %d: %f, N_diff = %d, mean_diff = %f', j, i, aggr, length(diff_bins), mean(diff_bins));
            
            figure;
            nbin = 15;
            
            [n1,x1]=hist(diff_bins,nbin);
            
            lim = max(x1);
            
            h = bar(x1*5/lim,n1,'facecolor',[0.0 0.0 0.0]);
                        
            lbls = {};
            lbls = arrayfun( @(i) sprintf('%d', i), 1:5, 'UniformOutput', 0);     
        
            % set(gca, 'DataAspectRatio', [1, lim(c)*1.25/1000, 1], 'XTick', (pos(1:2:2*g_config.TRIALS - 1) + pos(2:2:2*g_config.TRIALS)) / 2, 'XTickLabel', lbls, 'Ylim', [0, lim(c)/1000], 'FontSize', 0.75*g_config.FONT_SIZE);
            set(gca, 'XTick', 1:5, 'XTickLabel', lbls, 'FontSize', 0.75*g_config.FONT_SIZE);
            set(gca, 'LineWidth', g_config.AXIS_LINE_WIDTH);   
                 
            ylabel('N', 'FontSize', 0.75*g_config.FONT_SIZE);
            xlabel('t [s]', 'FontSize', g_config.FONT_SIZE);        
        
            set(gcf, 'Color', 'w');
            box off;  
            set(gcf,'papersize',[8,8], 'paperposition',[0,0,8,8]);

            export_figure(1, gcf, g_config.OUTPUT_DIR, sprintf('results_agreement_%d_%d', i, j));    
        end
    end    
end

